#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <mpi.h>

int main(){
    int n,n2,n3;
    int bandera = 0, i1,j1,i2,j2;
    int cont1=0, cont2=0;
    int rank,size;
    srand(time(NULL));
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    
    if(rank==0){
        
        while (bandera==0){
            printf("Ingrese un numero entre 300 y 400: \n");
            scanf("%d", &n);
            if (n>=300 && n<=400){
                bandera = 1;

                for (int i = 0; i < size; i++){
                    MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                }
            }else{
                printf("Ingrese un numero que este entre 300 y 400\n");
           }
        }
         
    }

    MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    if(n % 2!=0){
        n2 = (int) (n/2) +1;
        n = n2*2;
    }else{
        n2=n/2;
    }
    n3=n*n;
    int matriz[n][n];
    int camino1[n], camino2[n];
    int aux1[n][n];
    if(rank==0){
        for (int i = 0; i < n2; i++){
            for (int j = 0; j < n; j++){
                if (i==j){
                    matriz[i][j]=0;
                }else{
                    int a = rand () % 100;
                    if (a >= 90){
                        matriz[i][j]=100000;
                    }else{
                        matriz[i][j]=a;
                    }
                }
            }
            
        }
        MPI_Send(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD);
    }
    if(rank==2){
        for (int i = n2; i < n; i++){
            for (int j = 0; j < n; j++){
                if (i==j){
                    aux1[i][j]=0;
                }else{
                    int a = rand () % 100;
                    if (a >= 90){
                        aux1[i][j]=100000;
                    }else{
                        aux1[i][j]=a;
                    }
                }
            }
        }
        MPI_Send(&aux1, n3,MPI_INT, 1, 0, MPI_COMM_WORLD);
    }

    if(rank==1){
        MPI_Recv(&matriz, n3, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&aux1, n3, MPI_INT, 2, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (int x=n2; x < n; x++){
            for (int y=0; y < n; y++){
                matriz[x][y] = aux1[x][y];
            }
        }

        MPI_Send(&matriz, n3, MPI_INT, 3, 0, MPI_COMM_WORLD);
    }

    if(rank==3){
        MPI_Recv(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        for (int k = 0; k < n; k++){
            for (int i = 0; i < n; i++){
                for (int j = 0; j < n; j++){
                    int dt = matriz[i][k] + matriz[k][j];
                    if (matriz[i][j] > dt){
                        matriz[i][j] = dt;
                    }
                }
            }
        }
        MPI_Send(&matriz, n3, MPI_INT, 2, 0, MPI_COMM_WORLD);
        MPI_Send(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD);
        
    }
    if(rank==2){
        MPI_Recv(&matriz, n3, MPI_INT, 3, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        for (int x=0; x < n2; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",matriz[x][y]);
            }
            printf("\n");
        }
    }

    if(rank==1){
        MPI_Recv(&matriz, n3, MPI_INT, 3, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        sleep(2);
        for (int x=n/2; x < n; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",matriz[x][y]);
            }
            printf("\n");
        }
        MPI_Send(&matriz, n3, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }


    if(rank==0){
        sleep(5);
        MPI_Recv(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        bandera=0;
        while (bandera==0)
        {
            printf("Ingrese las coordenadas de origen: \n");
            scanf("%d %d", &i1,&j1);
            printf("Ingrese las coordenadas de destino: \n");
            scanf("%d %d", &i2,&j2);
            printf("Origen [%d,%d]\n",i1,j1);
            printf("Destino [%d,%d]\n",i2,j2);

            if (i1>=0 && i1<=(n-1) && j1>=0 && j1<=(n-1) && j2<n && i2<n ){
                
                if (i1==i2){
                    int val = 0;
                    int i = i1, j= j1;
                    if(j1<j2){
                        while (val==0){
                            camino1[cont1] = matriz[i][j];
                            if(j==j2)
                                val=1;
                            j++;
                            cont1++;
                        }
                    }else if(j1>j2){
                        while (val==0){
                            camino1[cont1] = matriz[i][j];
                            if(j==j2)
                                val=1;
                            j--;
                            cont1++;
                        }
                    }
                    
                }else if (j1==j2){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2)
                            val=1;
                        i++;
                        cont1++;
                    } 
                }else if (i1==j1 && i2==j2){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        cont1++;
                    }
                }else if(i1<j1 && i2<j2 && (j1-i1)==(j2-i2)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        cont1++;
                    }
                }else if(i1>j1 && i2>j2 && (i1-j1)==(i2-j2)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        cont1++;
                    }
                }else if((i1+j1) == (n-1) && (i2+j2)== (n-1)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j--;
                        cont1++;
                    }
                }else if((i1+j1)==(i2+j2)){
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j--;
                        cont1++;
                    }
                }else{
                    int i = i1, j= j1;
                    if (j<j2){
                        int val =0;
                        while (val==0){
                            camino2[cont2] = matriz[i][j];
                            if(i==i2){
                                if(j<j2){ 
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2=1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j++;
                                    }
                                }else if(j>j2){
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2= 1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j--;
                                    }
                                }
                            }else if(j==j2){
                                int val2 = 0;
                                while (val2==0){
                                    camino2[cont2] = matriz[i][j];
                                    if(i==i2 && j==j2){
                                        val2= 1;
                                        val = 1;
                                    }
                                    cont2++;
                                    i++;
                                }
                            }else{
                                cont2++;
                                i++;
                                j++;
                            }
                            
                        }
                    }else if(j>j2){
                        int val =0;
                        while (val==0){
                            camino2[cont2] = matriz[i][j];
                            if(i==i2){
                                if(j<j2){ 
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2=1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j++;
                                    }
                                }else if(j>j2){
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2= 1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j--;
                                    }
                                }
                            }else if(j==j2){
                                int val2 = 0;
                                while (val2==0){
                                    camino2[cont2] = matriz[i][j];
                                    if(i==i2 && j==j2){
                                        val2= 1;
                                        val = 1;
                                    }
                                    cont2++;
                                    i++;
                                }
                            }else{
                                cont2++;
                                i++;
                                j--;
                            }
                            
                        }
                    }
                }
                
                printf("Origen: %d  Destino: %d  Camino: ",matriz[i1][j1], matriz[i2][j2]);
                if(cont1>cont2){                
                    for (int i = 0; i < cont1; i++){
                        printf(" %d -> ", camino1[i]);
                    }
                    printf("\n");
                }else{
                    for (int i = 0; i < cont2; i++){
                        printf(" %d -> ", camino2[i]);
                    }
                    printf("\n");
                }    

                bandera = 1;
            }else{
                printf("Ingrese el rango de coordenadas validas\n");
            }

        }
    }

    MPI_Finalize();
    return 0;
}